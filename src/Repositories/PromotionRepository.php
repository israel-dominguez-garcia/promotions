<?php namespace Genetsis\Promotions\Repositories;

use Genetsis\Promotions\Contracts\PromotionParticipationInterface;
use Genetsis\Promotions\Models\Participation;
use Genetsis\Promotions\Models\Promotion;

class PromotionRepository {

    public function checkUserHasExtraParticipations($user_id, Promotion $promotion) {
        if (ExtraParticipation::where('promo_id', $promotion->id)->where('user_id', $user_id)->where('used', null)->count()>0) {
            return true;
        }
        return false;
    }

}