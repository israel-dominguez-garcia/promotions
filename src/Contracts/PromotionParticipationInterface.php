<?php namespace Genetsis\Promotions\Contracts;

interface PromotionParticipationInterface {
    public function participate();
}