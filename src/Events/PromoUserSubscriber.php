<?php namespace Genetsis\Promotions\Events;

use Genetsis\Promotions\Contracts\PromoUserEmailInterface;
use Genetsis\Promotions\Contracts\PromoUserInterface;
use Genetsis\Promotions\Models\User;

class PromoUserSubscriber
{

    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {
        echo 'Login';
        var_dump($event);
    }

    /**
     * Handle user logout events.
     */
    public function onUserCreated(PromoUserInterface $new_promo_user) {

        try {
            $promo_user = new User();
            $promo_user->id = $new_promo_user->getId();
            $promo_user->name = ($new_promo_user instanceof PromoUserEmailInterface) ? $new_promo_user->getName() : '';
            $promo_user->email = ($new_promo_user instanceof PromoUserEmailInterface) ? $new_promo_user->getEmail() : '';
            $promo_user->sponsor_code = hash('crc32',$promo_user->id, false);

            $promo_user->save();
            \Log::info('Promo User Created - sponsor code: ' . $promo_user->sponsor_code);
        } catch (\Exception $e) {
            \Log::warning('Exception: '.$e->getMessage());
        }
    }


    public function subscribe($events)
    {
        $events->listen(
            PromoUserCreated::class,
            PromoUserSubscriber::class.'@onUserLogin'
        );

        $events->listen('promouser.created', PromoUserSubscriber::class.'@onUserCreated');

    }

}
