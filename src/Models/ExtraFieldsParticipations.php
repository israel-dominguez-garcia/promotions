<?php namespace Genetsis\Promotions\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraFieldsParticipations extends Model
{
    protected $table = 'promo_extra_fields_participations';

    protected $fillable = ['participation_id', 'key', 'value'];

    protected $primaryKey = ['participation_id', 'key'];
    public $incrementing = false;
    public $timestamps = false;

}
