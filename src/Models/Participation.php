<?php

namespace Genetsis\Promotions\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promo_participations';

    protected $fillable = ['user_id', 'promo_id', 'sponsor', 'origin'];


    //protected $primaryKey = ['oid', 'promocode_id'];
    //public $incrementing = false;
    public $timestamps = false;

    public function moment() {
        return $this->hasOne(Moment::class);
    }

    public function promo() {
        return $this->belongsTo(Promotion::class, 'promo_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Before save a participation, check if user is consuming an extra participation to use it
        Participation::saving(function ($participation) {

        });
    }

}
