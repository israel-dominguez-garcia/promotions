@extends('layouts.app')

@section('content')
    @include('promotion::partials.section-header')

    @yield('section-content')
@endsection
