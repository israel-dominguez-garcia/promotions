@extends('promotion::layouts.admin-sections')

@section('section-content')
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Promotion: {{ $promotion->name }}</h2>
        </div>

        <div class="card-block">
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-expanded="true">Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-expanded="false">Participations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages" role="tab">Pincodes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Win Moments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#statistics" role="tab">Statistics</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade active show" id="home" role="tabpanel" aria-expanded="true">
                        Number of Participations: {{ count($promotion->participations) }}
                        <br>
                        Unique Users Participations: {{ $unique_users }}
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-expanded="false">
                        <table id="data-participations" class="table table-bordered table-striped">
                            <thead class="thead-inverse">
                                <tr>
                                    <td>#</td>
                                    <td>User ID</td>
                                    <td>Email</td>
                                    <td>Date</td>
                                    <td>Sponsor</td>
                                    <td>Origin</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="messages" role="tabpanel">
                        <p>Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis. Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat. Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.Morbi mattis ullamcorper velit. Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis. Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat. Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.</p>
                    </div>
                    <div class="tab-pane fade" id="statistics" role="tabpanel">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="card-title">Participations</h2>
                                    </div>
                                    <div class="card-block">
                                        <div class="flot-chart" id="participationsChart"></div>
                                        {{--<div class="flot-chart-legends flot-chart-legends--bar"></div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="card-title">Participations By WeekDay</h2>
                                    </div>
                                    <div class="card-block">
                                        <div class="flot-chart" id="weekdaysChart"></div>
                                        {{--<div class="flot-chart-legends flot-chart-legends--bar"></div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="card-title">Participations By Hours</h2>
                                    </div>
                                    <div class="card-block">
                                        <div class="flot-chart" id="hoursChart"></div>
                                        {{--<div class="flot-chart-legends flot-chart-legends--bar"></div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="btn btn-danger btn--icon-text waves-effect pull-2" href="{{ route('promotions.home') }}"><i class="zmdi zmdi-arrow-back"></i> Back</a>
                </li>
            </ul>
        </div>

    </div>
@endsection

@section('custom-js')

    <script>
        $(document).ready(function () {
            $('#data-participations').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/participations/{{$promotion->id}}',
                columns: [
                    {data: 'id'},
                    {data: 'user_id'},
                    {data: 'user.email'},
                    {data: 'date'},
                    {data: 'sponsor'},
                    {data: 'origin'}
                ]
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                if ($(e.target).attr("href") == "#statistics") {
                    var weekDayParticipations = [];
                    <?php foreach ($days as $key => $dayParticipation) { ?>
                    weekDayParticipations.push(["<?php echo $key; ?>", "<?php echo $dayParticipation; ?>"]);
                    <?php } ?>

                    // === Make WeekDays chart === //
                    var weekdaysChart = $.plot($("#weekdaysChart"),
                        [{data: weekDayParticipations, label: "Participations", color: "#4fabd2"}], {
                            series: {
                                bars: {show: true, barWidth: 0.5, fill: 1},
                            },
                            legend: {backgroundOpacity: 0.5},
                            grid: {
                                hoverable: true,
                                clickable: true,
                                borderColor: "#eeeeee",
                                borderWidth: 1,
                                color: "#AAAAAA"
                            },
                            yaxis: {min: 0, minTickSize: 1, tickDecimals: 0},
                            xaxis: {
                                mode: "time",
                                tickSize: [1, "day"],
                                timeformat: "%a",
                                ticks: [[1, "Mon"], [2, "Tue"], [3, "Wed"], [4, "Thu"], [5, "Fri"], [6, "Sat"], [0, "Sun"]]
                            },
                        });


                    var hourParticipations = [];
                    <?php foreach ($hours as $key => $hourParticipation) { ?>
                    hourParticipations.push([<?php echo $key;?>, <?php echo $hourParticipation; ?>]);
                    <?php } ?>


                    // === Make Hours chart === //
                    var hoursChart = $.plot($("#hoursChart"),
                        [{data: hourParticipations, label: "Participations", color: "#4fabd2"}], {
                            series: {
                                bars: {show: true},
                            },
                            legend: {backgroundOpacity: 0.5},
                            grid: {
                                hoverable: true,
                                clickable: true,
                                borderColor: "#eeeeee",
                                borderWidth: 1,
                                color: "#AAAAAA"
                            },
                            yaxis: {min: 0, minTickSize: 1, tickDecimals: 0},
                            xaxis: {tickSize: 1, minTickSize: 1, tickDecimals: 0},
                        });


                    var participations = [];
                    <?php foreach ($participations as $key => $participation) { ?>
                    participations.push([<?php echo $key;?>, <?php echo $participation; ?>]);
                    <?php } ?>


                    // === Make Hours chart === //
                    var participationsChart = $.plot($("#participationsChart"), [{
                        data: participations,
                        label: "Participations",
                        color: "#4fabd2"
                    }], {
                        legend: {backgroundOpacity: 0.5},
                        grid: {
                            hoverable: true,
                            clickable: true,
                            borderColor: "#eeeeee",
                            borderWidth: 1,
                            color: "#AAAAAA"
                        },
                        yaxis: {min: 0, minTickSize: 1, tickDecimals: 0},
                        xaxis: {
                            mode: "time",
                            timeformat: "%Y/%m/%d",
                            minTickSize: [1, "day"]
                        }
                    });


                    // Tooltips for Flot Charts
                    if ($('.flot-chart')[0]) {
                        $('.flot-chart').bind('plothover', function (event, pos, item) {
                            if (item) {
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);
                                $('.flot-tooltip').html(item.series.label + ' of ' + x + ' = ' + y).css({
                                    top: item.pageY + 5,
                                    left: item.pageX + 5
                                }).show();
                            }
                            else {
                                $('.flot-tooltip').hide();
                            }
                        });

                        $('<div class="flot-tooltip"></div>').appendTo('body');
                    }

                }
            });
        });


    </script>

@endsection
