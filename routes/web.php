<?php

Route::group(['middleware' => ['web']], function () {
    Route::prefix('admin')->group(function () {
        Route::group(['namespace' => 'Genetsis\Promotions\Controllers'], function () {
            Route::resource('campaigns', 'CampaignsController', [
                'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy'],
                'names' => ['index' => 'campaigns.home']
            ]);
            Route::resource('promotions', 'PromotionsController', [
                'only' => ['index', 'show', 'edit', 'create', 'store', 'update', 'destroy'],
                'names' => ['index' => 'promotions.home']
            ]);
        });
    });
});



